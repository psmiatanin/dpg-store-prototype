﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DPG.Store.Domain.v2
{
    public class StoreContacts
    {
        public List<PhoneNumber> Phones { get; } = new List<PhoneNumber>();

        public List<string> Faxes { get; } = new List<string>();

        public string Email { get; set; }
    }
}
