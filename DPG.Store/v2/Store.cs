﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace DPG.Store.Domain.v2
{
    public class Store
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        /// <summary>
        /// CURRENT operational status
        /// </summary>
        public StoreStatuses Status { get; set; }

        public string StatusMessage { get; set; }

        /// <summary>
        /// CURRENT trading type
        /// </summary>
        public StoreTradingTypes TradingType { get; set; }

        public string WelcomeMessage { get; set; }

        public Location Location { get; set; }

        public StoreContacts Contacts { get; set; }

        public StorePaymentConfiguration PaymentConfiguration { get; set; }

        public StoreDeliveryConfiguration DeliveryConfiguration { get; set; }

        public StoreWeekTradingHours WeekTradingHours { get; set; }

        /// <summary>
        /// CURRENT trading type
        /// </summary>
        public StoreTradingTypes TradingType
        {
            get
            {
                // TODO: calculation is required

                return StoreTradingTypes.DeliveryAndCollection;
            }
        }

        public StoreFulfilmentMethodFailureReasons CheckIfStoreFulfilmentMethodAllowed(StoreFulfilmentMethods method, DateTimeOffset? datetime)
        {
            datetime = datetime ?? DateTimeOffset.Now;

            // TODO: calculation is here
            StoreDayTradingHours tradingDay = null;

            switch (datetime.Value.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    tradingDay = WeekTradingHours.Monday;
                    break;
                default:
                    break;
            }

            throw new NotImplementedException();
        }
    }
}
