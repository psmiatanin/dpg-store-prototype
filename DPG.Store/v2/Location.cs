﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DPG.Store.Domain.v2
{
    public class Location
    {
        public GeoCoordinates Coordinates { get; set; }

        public Address Address { get; set; }
    }
}
