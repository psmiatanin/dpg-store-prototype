﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DPG.Store.Domain.v2
{
    public class StoreDeliveryConfiguration
    {
        public int MaxDeliveryTimeInMinutes { get; set; }

        public int AvgDeliveryTimeInMinutes { get; set; }

        public int MaxCollectionTimeInMinutes { get; set; }

        public int AvgCollectionTimeInMinutes { get; set; }
    }
}
