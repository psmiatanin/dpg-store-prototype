﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DPG.Store.Domain.v2
{
    public class StorePaymentConfiguration
    {
        /// <summary>
        /// Currency code based on ISO-4217. For example EUR, USD and so on.
        /// Currency symbol can be calculated based on currency code.
        /// </summary>
        public string CurrencyCode { get; set; }

        public decimal? MinOrderPaymentAmountForDelivery { get; set; }

        public decimal? MaxPaymentAmountByCash { get; set; }

        public decimal? MaxPaymentAmountByCard { get; set; }

        public decimal? MaxPaymentAmountByPayPal { get; set; }

        public TimeSpan? CashEndTime { get; set; }

        public bool PenniesEnabled { get; set; }
    }
}
