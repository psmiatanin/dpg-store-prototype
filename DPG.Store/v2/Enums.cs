﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DPG.Store.Domain.v2
{
    public enum StoreStatuses
    {
        Online,
        Offline
    }

    public enum StoreTradingTypes
    {
        PreOrder,
        DeliveryOnly,
        DeliveryAndCollection,
        CollectionOnly,
        Closed
    }

    public enum StoreFulfilmentMethods
    {
        Collection,
        Delivery
    }
    public enum StoreFulfilmentMethodFailureReasons
    {
        None,
        StoreIsClosed,
        StoreIsOffline,
        DeliveryOnly,
        CollectionOnly
    }

    public enum PhoneNumberCategories
    {
        Generic,
        CustomerClaims
    }
}
