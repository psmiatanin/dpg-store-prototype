﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DPG.Store.Domain.v2
{
    public struct GeoCoordinates : IEquatable<GeoCoordinates>
    {
        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public bool Equals(GeoCoordinates other)
        {
            return other.Latitude == Latitude && other.Longitude == Longitude;
        }

        public override bool Equals(object obj)
        {
            return (obj == null || obj.GetType() != typeof(GeoCoordinates)) ? false
                : Equals((GeoCoordinates)obj);
        }

        public override int GetHashCode()
        {
            throw new NotImplementedException();
        }
    }
}
