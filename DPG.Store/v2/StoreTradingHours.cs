﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DPG.Store.Domain.v2
{
    public struct StoreTradingHours : IEquatable<StoreTradingHours>
    {
        public TimeSpan Start { get; set; }

        public TimeSpan End { get; set; }

        public StoreTradingTypes TradingType { get; set; }

        public bool Equals(StoreTradingHours other)
        {
            return other.Start == Start && other.End == End && other.TradingType == TradingType;
        }
    }
}
