﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DPG.Store.Domain.v2
{
    public class PhoneNumber
    {
        public string Number { get; set; }

        public PhoneNumberCategories Category { get; set; }

        public string Description { get; set; }
    }
}
