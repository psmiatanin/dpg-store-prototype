﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DPG.Store.Domain.v2
{
    public class StoreWeekTradingHours
    {
        public StoreDayTradingHours Monday { get; } = new StoreDayTradingHours();

        public StoreDayTradingHours Tuesday { get; } = new StoreDayTradingHours();

        public StoreDayTradingHours Wednsday { get; } = new StoreDayTradingHours();

        public StoreDayTradingHours Thursday { get; } = new StoreDayTradingHours();

        public StoreDayTradingHours Friday { get; } = new StoreDayTradingHours();

        public StoreDayTradingHours Saturday { get; } = new StoreDayTradingHours();

        public StoreDayTradingHours Sunday { get; } = new StoreDayTradingHours();
    }
}
