﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DPG.Store.WebApi.Model
{
    public enum StoreFulfilmentFailureReasons
    {
        StoreIsClosed,
        StoreIsOffline,
        DeliveryOnly,
        CollectionOnly
    }
}
